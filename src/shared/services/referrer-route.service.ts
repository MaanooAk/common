import {Injectable} from '@angular/core';
import { DefaultUrlSerializer, NavigationEnd, NavigationStart, PRIMARY_OUTLET, Router, UrlSegmentGroup } from '@angular/router';
import { BehaviorSubject, Observable } from 'rxjs';
import { filter, first, map } from 'rxjs/operators';

export interface ReferrerRouteParams {
  commands: string[];
  queryParams?: any;
  fragment?: string;
}

/**
 *
 * Displays Loading Spinner
 * @export
 * @class LoadingService
 */
@Injectable()
export class ReferrerRouteService {

  public referrer$ = new BehaviorSubject<string>(null);
  private _referrer: string;

  constructor(private router: Router) {
    router.events
    .pipe(filter(event => event instanceof NavigationEnd || event instanceof NavigationStart))
    .subscribe((event: NavigationStart | NavigationEnd) => {
        if (event instanceof NavigationStart) {
          if (router.routerState.snapshot && router.routerState.snapshot.url.length) {
            this._referrer = router.routerState.snapshot.url;
          } else {
            this._referrer = null;
          }
        } else if (event instanceof NavigationEnd) {
          if (this._referrer != null && this._referrer !== event.urlAfterRedirects) {
            this.referrer$.next(this._referrer);
            this._referrer = null;
          }
        }
    });
  }

  public get first$(): Observable<string> {
    return this.referrer$.pipe(first());
  }

  public get routeParams$(): Observable<ReferrerRouteParams> {
    return this.referrer$.pipe(first()).pipe(map((url: string) => {
      if (url) {
        const urlSerializer = new DefaultUrlSerializer();
        const tree = urlSerializer.parse(url);
        const queryParams = tree.queryParams;
        tree.queryParams = {};
        const fragment = tree.fragment;
        tree.fragment = null;
        tree.queryParams = {};
        const g: UrlSegmentGroup = tree.root.children[PRIMARY_OUTLET];
        const commands = g.segments.map((segment) => segment.path);
        commands.splice(0, 0, '/');
        return {
          commands,
          queryParams,
          fragment
        } as ReferrerRouteParams;
      }
    }));
  }

}
